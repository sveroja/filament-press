EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "filament press"
Date "2021-09-03"
Rev "0.9"
Comp "biat, Europa-Universität Flensburg"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM358 U3
U 1 1 6126ADDB
P 2300 1350
F 0 "U3" H 2300 1717 50  0000 C CNN
F 1 "LM358" H 2300 1626 50  0000 C CNN
F 2 "" H 2300 1350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2300 1350 50  0001 C CNN
	1    2300 1350
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:PT1000 TH2
U 1 1 61275842
P 1500 1650
F 0 "TH2" H 1598 1696 50  0000 L CNN
F 1 "PT1000" H 1598 1605 50  0000 L CNN
F 2 "" H 1500 1700 50  0001 C CNN
F 3 "https://www.heraeus.com/media/media/group/doc_group/products_1/hst/sot_to/de_15/to_92_d.pdf" H 1500 1700 50  0001 C CNN
	1    1500 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 61276627
P 1500 1050
F 0 "R1" H 1570 1096 50  0000 L CNN
F 1 "10k" H 1570 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1430 1050 50  0001 C CNN
F 3 "~" H 1500 1050 50  0001 C CNN
	1    1500 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1200 1500 1250
$Comp
L Device:R R2
U 1 1 61287335
P 1150 1050
F 0 "R2" H 1220 1096 50  0000 L CNN
F 1 "10k" H 1220 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 1050 50  0001 C CNN
F 3 "~" H 1150 1050 50  0001 C CNN
	1    1150 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 612CEFBA
P 1150 1650
F 0 "R?" H 1220 1696 50  0000 L CNN
F 1 "1k" H 1220 1605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 1650 50  0001 C CNN
F 3 "~" H 1150 1650 50  0001 C CNN
	1    1150 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1200 1150 1450
Wire Wire Line
	2000 1250 1500 1250
Connection ~ 1500 1250
Wire Wire Line
	1500 1250 1500 1500
Connection ~ 1150 1450
Wire Wire Line
	1150 1450 1150 1500
$Comp
L Device:R R?
U 1 1 612D5D4E
P 2300 1700
F 0 "R?" V 2093 1700 50  0000 C CNN
F 1 "R" V 2184 1700 50  0000 C CNN
F 2 "" V 2230 1700 50  0001 C CNN
F 3 "~" H 2300 1700 50  0001 C CNN
	1    2300 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 1450 1950 1450
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 6130B829
P 2300 2650
F 0 "U?" H 2300 3017 50  0000 C CNN
F 1 "LM358" H 2300 2926 50  0000 C CNN
F 2 "" H 2300 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2300 2650 50  0001 C CNN
	1    2300 2650
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:PT1000 TH?
U 1 1 6130BA9F
P 1500 2950
F 0 "TH?" H 1598 2996 50  0000 L CNN
F 1 "PT1000" H 1598 2905 50  0000 L CNN
F 2 "" H 1500 3000 50  0001 C CNN
F 3 "https://www.heraeus.com/media/media/group/doc_group/products_1/hst/sot_to/de_15/to_92_d.pdf" H 1500 3000 50  0001 C CNN
	1    1500 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6130BAA9
P 1500 2350
F 0 "R?" H 1570 2396 50  0000 L CNN
F 1 "10k" H 1570 2305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1430 2350 50  0001 C CNN
F 3 "~" H 1500 2350 50  0001 C CNN
	1    1500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2500 1500 2550
$Comp
L Device:R R?
U 1 1 6130BAB4
P 1150 2350
F 0 "R?" H 1220 2396 50  0000 L CNN
F 1 "10k" H 1220 2305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 2350 50  0001 C CNN
F 3 "~" H 1150 2350 50  0001 C CNN
	1    1150 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6130BABE
P 1150 2950
F 0 "R?" H 1220 2996 50  0000 L CNN
F 1 "1k" H 1220 2905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 2950 50  0001 C CNN
F 3 "~" H 1150 2950 50  0001 C CNN
	1    1150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2500 1150 2750
Wire Wire Line
	2000 2550 1500 2550
Connection ~ 1500 2550
Wire Wire Line
	1500 2550 1500 2800
Connection ~ 1150 2750
Wire Wire Line
	1150 2750 1150 2800
$Comp
L Device:R R?
U 1 1 6130BACE
P 2300 3000
F 0 "R?" V 2093 3000 50  0000 C CNN
F 1 "R" V 2184 3000 50  0000 C CNN
F 2 "" V 2230 3000 50  0001 C CNN
F 3 "~" H 2300 3000 50  0001 C CNN
	1    2300 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 2750 1950 2750
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 61322EC1
P 2300 3950
F 0 "U?" H 2300 4317 50  0000 C CNN
F 1 "LM358" H 2300 4226 50  0000 C CNN
F 2 "" H 2300 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2300 3950 50  0001 C CNN
	1    2300 3950
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:PT1000 TH?
U 1 1 6132323B
P 1500 4250
F 0 "TH?" H 1598 4296 50  0000 L CNN
F 1 "PT1000" H 1598 4205 50  0000 L CNN
F 2 "" H 1500 4300 50  0001 C CNN
F 3 "https://www.heraeus.com/media/media/group/doc_group/products_1/hst/sot_to/de_15/to_92_d.pdf" H 1500 4300 50  0001 C CNN
	1    1500 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61323245
P 1500 3650
F 0 "R?" H 1570 3696 50  0000 L CNN
F 1 "10k" H 1570 3605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1430 3650 50  0001 C CNN
F 3 "~" H 1500 3650 50  0001 C CNN
	1    1500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3800 1500 3850
$Comp
L Device:R R?
U 1 1 61323250
P 1150 3650
F 0 "R?" H 1220 3696 50  0000 L CNN
F 1 "10k" H 1220 3605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 3650 50  0001 C CNN
F 3 "~" H 1150 3650 50  0001 C CNN
	1    1150 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6132325A
P 1150 4250
F 0 "R?" H 1220 4296 50  0000 L CNN
F 1 "1k" H 1220 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 4250 50  0001 C CNN
F 3 "~" H 1150 4250 50  0001 C CNN
	1    1150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3800 1150 4050
Wire Wire Line
	2000 3850 1500 3850
Connection ~ 1500 3850
Wire Wire Line
	1500 3850 1500 4100
Connection ~ 1150 4050
Wire Wire Line
	1150 4050 1150 4100
$Comp
L Device:R R?
U 1 1 6132326A
P 2300 4300
F 0 "R?" V 2093 4300 50  0000 C CNN
F 1 "R" V 2184 4300 50  0000 C CNN
F 2 "" V 2230 4300 50  0001 C CNN
F 3 "~" H 2300 4300 50  0001 C CNN
	1    2300 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 4050 1950 4050
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 61323275
P 2300 5250
F 0 "U?" H 2300 5617 50  0000 C CNN
F 1 "LM358" H 2300 5526 50  0000 C CNN
F 2 "" H 2300 5250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2300 5250 50  0001 C CNN
	1    2300 5250
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:PT1000 TH?
U 1 1 6132327F
P 1500 5550
F 0 "TH?" H 1598 5596 50  0000 L CNN
F 1 "PT1000" H 1598 5505 50  0000 L CNN
F 2 "" H 1500 5600 50  0001 C CNN
F 3 "https://www.heraeus.com/media/media/group/doc_group/products_1/hst/sot_to/de_15/to_92_d.pdf" H 1500 5600 50  0001 C CNN
	1    1500 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61323289
P 1500 4950
F 0 "R?" H 1570 4996 50  0000 L CNN
F 1 "10k" H 1570 4905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1430 4950 50  0001 C CNN
F 3 "~" H 1500 4950 50  0001 C CNN
	1    1500 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 5100 1500 5150
$Comp
L Device:R R?
U 1 1 61323294
P 1150 4950
F 0 "R?" H 1220 4996 50  0000 L CNN
F 1 "10k" H 1220 4905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 4950 50  0001 C CNN
F 3 "~" H 1150 4950 50  0001 C CNN
	1    1150 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6132329E
P 1150 5550
F 0 "R?" H 1220 5596 50  0000 L CNN
F 1 "1k" H 1220 5505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1080 5550 50  0001 C CNN
F 3 "~" H 1150 5550 50  0001 C CNN
	1    1150 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5100 1150 5350
Wire Wire Line
	2000 5150 1500 5150
Connection ~ 1500 5150
Wire Wire Line
	1500 5150 1500 5400
Connection ~ 1150 5350
Wire Wire Line
	1150 5350 1150 5400
$Comp
L Device:R R?
U 1 1 613232AE
P 2300 5600
F 0 "R?" V 2093 5600 50  0000 C CNN
F 1 "R" V 2184 5600 50  0000 C CNN
F 2 "" V 2230 5600 50  0001 C CNN
F 3 "~" H 2300 5600 50  0001 C CNN
	1    2300 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 5350 1950 5350
$Comp
L Device:R_POT RV?
U 1 1 613DA4C9
P 1150 6200
F 0 "RV?" H 1081 6246 50  0000 R CNN
F 1 "R_POT" H 1081 6155 50  0000 R CNN
F 2 "" H 1150 6200 50  0001 C CNN
F 3 "~" H 1150 6200 50  0001 C CNN
	1    1150 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 613DDDD4
P 1150 6600
F 0 "RV?" H 1081 6646 50  0000 R CNN
F 1 "R_POT" H 1081 6555 50  0000 R CNN
F 2 "" H 1150 6600 50  0001 C CNN
F 3 "~" H 1150 6600 50  0001 C CNN
	1    1150 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 613DEAB5
P 1150 7000
F 0 "RV?" H 1081 7046 50  0000 R CNN
F 1 "R_POT" H 1081 6955 50  0000 R CNN
F 2 "" H 1150 7000 50  0001 C CNN
F 3 "~" H 1150 7000 50  0001 C CNN
	1    1150 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 613DF506
P 1150 7400
F 0 "RV?" H 1081 7446 50  0000 R CNN
F 1 "R_POT" H 1081 7355 50  0000 R CNN
F 2 "" H 1150 7400 50  0001 C CNN
F 3 "~" H 1150 7400 50  0001 C CNN
	1    1150 7400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 613E21F6
P 3400 6800
F 0 "U?" H 3450 7481 50  0000 C CNN
F 1 "74HC4051" H 3450 7390 50  0000 C CNN
F 2 "" H 3400 6400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 3400 6400 50  0001 C CNN
	1    3400 6800
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 61435354
P 3350 3300
F 0 "U?" H 3400 3981 50  0000 C CNN
F 1 "74HC4051" H 3400 3890 50  0000 C CNN
F 2 "" H 3350 2900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 3350 2900 50  0001 C CNN
	1    3350 3300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2600 1350 2900 1350
Wire Wire Line
	2900 1350 2900 3000
Wire Wire Line
	2900 3000 2950 3000
Wire Wire Line
	2600 5250 2900 5250
Wire Wire Line
	2900 5250 2900 3300
Wire Wire Line
	2900 3300 2950 3300
Wire Wire Line
	2600 2650 2800 2650
Wire Wire Line
	2800 2650 2800 3100
Wire Wire Line
	2800 3100 2950 3100
Wire Wire Line
	2600 3950 2800 3950
Wire Wire Line
	2800 3950 2800 3200
Wire Wire Line
	2800 3200 2950 3200
Wire Wire Line
	1300 6200 2900 6200
Wire Wire Line
	2900 6200 2900 6500
Wire Wire Line
	2900 6500 3000 6500
Wire Wire Line
	1300 7400 2900 7400
Wire Wire Line
	2900 7400 2900 6800
Wire Wire Line
	2900 6800 3000 6800
Wire Wire Line
	1300 6600 3000 6600
Wire Wire Line
	1300 7000 2800 7000
Wire Wire Line
	2800 7000 2800 6700
Wire Wire Line
	2800 6700 3000 6700
Wire Wire Line
	3650 3000 3800 3000
Wire Wire Line
	8850 3400 8850 4100
Wire Wire Line
	8850 6500 3700 6500
Wire Wire Line
	3650 3200 4200 3200
Wire Wire Line
	3650 3300 4550 3300
Wire Wire Line
	4550 3300 4550 2800
Wire Wire Line
	3700 6700 5850 6700
Wire Wire Line
	5850 6700 5850 3000
Wire Wire Line
	3700 6800 6000 6800
Wire Wire Line
	6000 6800 6000 2700
Wire Wire Line
	3650 3400 3650 3600
Connection ~ 3650 3600
Wire Wire Line
	3650 3600 3650 3900
Connection ~ 3350 3900
Wire Wire Line
	3350 3900 3250 3900
Wire Wire Line
	3700 7100 3700 7400
Wire Wire Line
	3700 7400 3400 7400
Connection ~ 3400 7400
Wire Wire Line
	3400 7400 3300 7400
Wire Wire Line
	3700 6900 3700 7100
Connection ~ 3700 7100
$Comp
L power:GND #PWR?
U 1 1 614802FD
P 3400 7550
F 0 "#PWR?" H 3400 7300 50  0001 C CNN
F 1 "GND" H 3405 7377 50  0000 C CNN
F 2 "" H 3400 7550 50  0001 C CNN
F 3 "" H 3400 7550 50  0001 C CNN
	1    3400 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 7550 3400 7400
$Comp
L power:GND #PWR?
U 1 1 6147F8B1
P 3350 4000
F 0 "#PWR?" H 3350 3750 50  0001 C CNN
F 1 "GND" H 3355 3827 50  0000 C CNN
F 2 "" H 3350 4000 50  0001 C CNN
F 3 "" H 3350 4000 50  0001 C CNN
	1    3350 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3900 3350 3900
Wire Wire Line
	3350 4000 3350 3900
Wire Wire Line
	2450 1700 2600 1700
Wire Wire Line
	2600 1700 2600 1350
Connection ~ 2600 1350
Wire Wire Line
	2150 1700 1950 1700
Wire Wire Line
	1950 1700 1950 1450
Connection ~ 1950 1450
Wire Wire Line
	1950 1450 2000 1450
Wire Wire Line
	2450 3000 2600 3000
Wire Wire Line
	2600 3000 2600 2650
Connection ~ 2600 2650
Wire Wire Line
	2150 3000 1950 3000
Wire Wire Line
	1950 3000 1950 2750
Connection ~ 1950 2750
Wire Wire Line
	1950 2750 2000 2750
Wire Wire Line
	2450 4300 2600 4300
Wire Wire Line
	2600 4300 2600 3950
Connection ~ 2600 3950
Wire Wire Line
	2150 4300 1950 4300
Wire Wire Line
	1950 4300 1950 4050
Connection ~ 1950 4050
Wire Wire Line
	1950 4050 2000 4050
Wire Wire Line
	2450 5600 2600 5600
Wire Wire Line
	2600 5600 2600 5250
Connection ~ 2600 5250
Wire Wire Line
	2150 5600 1950 5600
Wire Wire Line
	1950 5600 1950 5350
Connection ~ 1950 5350
Wire Wire Line
	1950 5350 2000 5350
$Comp
L power:+5V #PWR?
U 1 1 6132BFE1
P 950 750
F 0 "#PWR?" H 950 600 50  0001 C CNN
F 1 "+5V" H 965 923 50  0000 C CNN
F 2 "" H 950 750 50  0001 C CNN
F 3 "" H 950 750 50  0001 C CNN
	1    950  750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  750  950  850 
Wire Wire Line
	950  4800 1150 4800
Wire Wire Line
	1500 4800 1150 4800
Connection ~ 1150 4800
Wire Wire Line
	1500 3500 1150 3500
Connection ~ 1150 3500
Wire Wire Line
	1150 3500 950  3500
Wire Wire Line
	1500 2200 1150 2200
Connection ~ 950  2200
Wire Wire Line
	950  2200 950  3500
Connection ~ 1150 2200
Wire Wire Line
	1150 2200 950  2200
Wire Wire Line
	1500 900  1150 900 
Connection ~ 950  900 
Wire Wire Line
	950  900  950  2200
Connection ~ 1150 900 
Wire Wire Line
	1150 900  950  900 
Connection ~ 950  3500
Wire Wire Line
	950  3500 950  4800
$Comp
L power:GND #PWR?
U 1 1 6138AA8B
P 600 7550
F 0 "#PWR?" H 600 7300 50  0001 C CNN
F 1 "GND" H 605 7377 50  0000 C CNN
F 2 "" H 600 7550 50  0001 C CNN
F 3 "" H 600 7550 50  0001 C CNN
	1    600  7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  7550 600  7150
Wire Wire Line
	600  1800 1150 1800
Connection ~ 1150 1800
Wire Wire Line
	1150 1800 1500 1800
Wire Wire Line
	600  3100 1150 3100
Connection ~ 1150 3100
Wire Wire Line
	1150 3100 1500 3100
Connection ~ 600  3100
Wire Wire Line
	600  3100 600  1800
Wire Wire Line
	1500 4400 1150 4400
Connection ~ 600  4400
Wire Wire Line
	600  4400 600  3100
Connection ~ 1150 4400
Wire Wire Line
	1150 4400 600  4400
Wire Wire Line
	1500 5700 1150 5700
Connection ~ 600  5700
Wire Wire Line
	600  5700 600  4400
Connection ~ 1150 5700
Wire Wire Line
	1150 5700 600  5700
Wire Wire Line
	750  5950 750  6050
Wire Wire Line
	750  6050 1150 6050
Wire Wire Line
	1150 6450 750  6450
Wire Wire Line
	750  6450 750  6050
Connection ~ 750  6050
Wire Wire Line
	1150 7250 750  7250
Wire Wire Line
	750  7250 750  6850
Connection ~ 750  6450
Wire Wire Line
	1150 6850 750  6850
Connection ~ 750  6850
Wire Wire Line
	750  6850 750  6450
Wire Wire Line
	1150 7550 600  7550
Connection ~ 600  7550
Wire Wire Line
	1150 7150 600  7150
Connection ~ 600  7150
Wire Wire Line
	600  7150 600  6750
Wire Wire Line
	1150 6750 600  6750
Connection ~ 600  6750
Wire Wire Line
	600  6750 600  6350
Wire Wire Line
	1150 6350 600  6350
Connection ~ 600  6350
Wire Wire Line
	600  6350 600  5700
Wire Wire Line
	7100 4400 7100 3500
Wire Wire Line
	7000 4500 7000 3400
Wire Wire Line
	6900 4600 6900 3300
Wire Wire Line
	6800 4700 6800 3200
Wire Wire Line
	8850 4300 8850 4100
$Comp
L power:+5V #PWR?
U 1 1 614F78A3
P 9050 4200
F 0 "#PWR?" H 9050 4050 50  0001 C CNN
F 1 "+5V" H 9065 4373 50  0000 C CNN
F 2 "" H 9050 4200 50  0001 C CNN
F 3 "" H 9050 4200 50  0001 C CNN
	1    9050 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3700 9250 4200
Wire Wire Line
	9250 4200 9050 4200
Wire Wire Line
	750  5950 3400 5950
Wire Wire Line
	5700 5950 5700 1550
Connection ~ 950  850 
Wire Wire Line
	950  850  950  900 
Wire Wire Line
	3350 2800 3350 1550
Wire Wire Line
	3350 1550 5700 1550
Connection ~ 5700 1550
Wire Wire Line
	3400 6300 3400 5950
Connection ~ 3400 5950
Wire Wire Line
	3400 5950 5700 5950
Wire Wire Line
	8850 1950 8850 3300
Wire Wire Line
	3800 3000 3800 1950
Wire Wire Line
	3800 1950 8850 1950
Wire Wire Line
	5850 3000 7500 3000
Wire Wire Line
	4200 2900 7500 2900
Wire Wire Line
	4550 2800 7500 2800
Wire Wire Line
	6000 2700 7500 2700
Wire Wire Line
	8100 4100 8000 4100
Connection ~ 8100 4100
Wire Wire Line
	8500 3400 8850 3400
Wire Wire Line
	8850 3300 8500 3300
Wire Wire Line
	7100 3500 7500 3500
Wire Wire Line
	7000 3400 7500 3400
Wire Wire Line
	6900 3300 7500 3300
Wire Wire Line
	6800 3200 7500 3200
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 61250DBA
P 8000 3100
F 0 "A1" H 8000 2011 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 8000 1920 50  0000 C CNN
F 2 "Module:Arduino_Nano_WithMountingHoles" H 8000 3100 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 8000 3100 50  0001 C CNN
	1    8000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4400 9600 4400
Wire Wire Line
	9600 4500 7000 4500
Wire Wire Line
	9600 4600 6900 4600
Wire Wire Line
	9600 4700 6800 4700
$Comp
L Connector:Conn_01x06_Male J?
U 1 1 6143CB9A
P 9800 4500
F 0 "J?" H 9772 4246 50  0000 R CNN
F 1 "Conn_01x06_Male" H 9772 4473 50  0001 R CNN
F 2 "" H 9800 4500 50  0001 C CNN
F 3 "~" H 9800 4500 50  0001 C CNN
F 4 "Relais1" H 9750 4400 50  0000 R CNN "Field1"
F 5 "Relais2" H 9750 4500 50  0000 R CNN "Field2"
F 6 "Relais3" H 9750 4600 50  0000 R CNN "Field3"
F 7 "Relais4" H 9750 4700 50  0000 R CNN "Field4"
	1    9800 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9600 3700 9250 3700
Wire Wire Line
	9600 3600 8500 3600
Wire Wire Line
	9600 3500 8500 3500
$Comp
L Connector:Screw_Terminal_01x04 J?
U 1 1 6150A168
P 9800 3600
F 0 "J?" H 9750 4000 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 9750 3900 50  0000 L CNN
F 2 "" H 9800 3600 50  0001 C CNN
F 3 "~" H 9800 3600 50  0001 C CNN
F 4 "SDA" H 10000 3700 50  0000 C CNN "Field1"
F 5 "SCL" H 10000 3600 50  0000 C CNN "Field2"
	1    9800 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 4300 9400 4300
Wire Wire Line
	9600 3800 9400 3800
Wire Wire Line
	9400 3800 9400 4300
Connection ~ 9400 4300
Wire Wire Line
	9400 4300 9600 4300
Connection ~ 9250 4200
Wire Wire Line
	9250 4200 9600 4200
Connection ~ 8850 4100
Connection ~ 8850 4300
Wire Wire Line
	8850 4300 8850 6500
Wire Wire Line
	8100 4100 8850 4100
Wire Wire Line
	7900 850  7900 2100
Wire Wire Line
	950  850  7900 850 
Wire Wire Line
	8100 1550 8100 2100
Wire Wire Line
	5700 1550 8100 1550
Wire Wire Line
	4200 3200 4200 2900
$EndSCHEMATC
