/*
	filament press

	This sketch reads four setpoints (voltages of potentiometers) per one multiplexer and four temperatures (voltages of wheatstone bridges) per another multiplexer. 
  After this information is displayed, the values are compared to set a two point regulator with some hysterese.

	The circuit:
	* sensorPin and potiPin are the outputs of the connected multiplexers.
	* SENSOR_ADDRESS_n and POTI_ADDRESS_n are the inputs of the multiplexers, which have to be triggered bitwise
  * RELAIS_PIN_n are the connected inputs of the relais module

	Created 15.08.21
	By Sven Jäger, biat, Europa-Universität Flensburg
	Modified day month year
	By author's name

*/

//#include <Wire.h>
#include <LiquidCrystal_I2C.h>
//#include <WiFiNINA.h>
//-----Hardware Adressierung-----//Bei falscher Funktion bitte obere Zeile auskommentie-ren,//und untere Zeile freigeben
LiquidCrystal_I2C lcd(0x27, 20, 4);
//LiquidCrystal_I2C lcd(0x3F,20,4);
unsigned long previousMillis = 0;
const long interval = 1000;

#define SENSOR_PIN A2       // where the multiplexer out port is connected
#define POTI_PIN A3         // where the multiplexer out port is connected
#define RELAIS_PIN_1 10     //where the 1st relais of the modul is connected
#define RELAIS_PIN_2 9      //where the 2nd relais of the modul is connected
#define RELAIS_PIN_3 8      //where the 3rd relais of the modul is connected
#define RELAIS_PIN_4 7      //where the 4th relais of the modul is connected
#define SENSOR_ADDRESS_0 4  // low-order bit
#define SENSOR_ADDRESS_1 3  // high-order bit
#define POTI_ADDRESS_0 5    // low-order bit
#define POTI_ADDRESS_1 2    // high-order bit

const byte hyst = 3;  //Hysterese
int potiAddressPins[] = { POTI_ADDRESS_0, POTI_ADDRESS_1 };
int sensorAddressPins[] = { SENSOR_ADDRESS_0, SENSOR_ADDRESS_1 };

int sollTemp[4];
int istTemp[4];
float AvgTemp[4];
float AvgTempVal[4];
int MeasurementsToAverage = 10;  // number of measures for average

int RelaisPins[4] = { RELAIS_PIN_1, RELAIS_PIN_2, RELAIS_PIN_3, RELAIS_PIN_4 };
void setup() {
  Serial.begin(9600);  // Öffnet die serielle Schnittstelle bei 9600 Bit/s:
  pinMode(SENSOR_ADDRESS_0, OUTPUT);
  pinMode(SENSOR_ADDRESS_1, OUTPUT);
  pinMode(POTI_ADDRESS_0, OUTPUT);
  pinMode(POTI_ADDRESS_1, OUTPUT);
  pinMode(RELAIS_PIN_1, OUTPUT);
  pinMode(RELAIS_PIN_2, OUTPUT);
  pinMode(RELAIS_PIN_3, OUTPUT);
  pinMode(RELAIS_PIN_4, OUTPUT);

  pinMode(SENSOR_PIN, INPUT);
  pinMode(POTI_PIN, INPUT);

  lcd.init();
  lcd.backlight();  // Hintergrundbeleuchtung an
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Filamentpresse");
  lcd.setCursor(0, 1);
  lcd.print("Version 0.93.1");
  lcd.setCursor(0, 2);
  lcd.print("ACHTUNG -");
  lcd.setCursor(0, 3);
  lcd.print("PRESSE WIRD WARM!");

  delay(5000);
  lcd.clear();

  for (int i = 0; i < 4; i++) {
    int row = switchDisplayOrder(i);

    lcd.setCursor(0, row);
    lcd.print("Temperatur ");
    lcd.print(i);
    lcd.print("=");
    lcd.setCursor(16, row);
    lcd.print("/");
  }
}

int switchDisplayOrder(int i) {
  int otherRow = 0;
  switch (i) {
    case 0:
      otherRow = 3;
      break;
    case 1:
      otherRow = 2;
      break;
    case 2:
      otherRow = 1;
      break;
    case 3:
      otherRow = 0;
      break;
  }
  return otherRow;
}

void display(int n, int tempist, int tempsoll) {
  int row = switchDisplayOrder(n);

  lcd.setCursor(13, row);

  if (tempist < 10) lcd.print(' ');
  if (tempist < 100) lcd.print(' ');
  lcd.print(tempist);
  lcd.setCursor(17, row);

  if (tempsoll < 10) lcd.print(' ');
  if (tempsoll < 100) lcd.print(' ');
  lcd.print(tempsoll);
}


int readPoti(const byte i) {
  // select correct Poti channel

  for (byte j = 0; j <= 2; j++) {
    digitalWrite(potiAddressPins[j], bitRead(i, j));
  }

  int potiReturn = analogRead(POTI_PIN);
  return potiReturn;
}  // end of readPoti



double readSensor(const byte i) {
  for (byte j = 0; j <= 2; j++) {
    digitalWrite(sensorAddressPins[j], bitRead(i, j));
  }

  int valReturn = analogRead(SENSOR_PIN);
  //Serial.println(valReturn);
  //double tempReturn = valReturn * 0.59 - 86.5;
  delay(10);
  return valReturn;
}  // end of readSensor


void loop() {
  unsigned long currentMillis = millis();            //current time after since reset
  if (currentMillis - previousMillis >= interval) {  //each interval time
    previousMillis = currentMillis;

    for (byte i = 0; i < 4; i++) {
      AvgTempVal[i] = 0;  //delete average every loop
    }

    for (byte j = 0; j < MeasurementsToAverage; j++) {
      for (byte i = 0; i < 4; i++) {


        AvgTempVal[i] += readSensor(i);  //sum up read temperatures
        delay(1);
      }
    }
    for (byte k = 0; k < 4; k++) {
      AvgTempVal[k] /= MeasurementsToAverage;  //devide sum by number of measurements
      Serial.print(AvgTempVal[k]);
      Serial.print("\t");
      //different types of simulating the function. Please comment out.

      //pow(base,exponent)
      //fourth degrees polynomial:
      //y = -6E-09x^4 + 1E-05x^3 - 0,0087x^2 + 3,2656x - 347,26
      AvgTemp[k] = 0.00000342110490700836 *pow(AvgTempVal[k],3)-0.00419469912730038 *pow(AvgTempVal[k],2)+2.27948319383230*AvgTempVal[k]-270.900187105664;
      //0.00000342110490700836 x^(3)-0.00419469912730038 x^(2)+2.27948319383230 x-270.900187105664
      //Linearization every 50°C:
      // 20 bis 50(205)°C:y = 1,06x - 168,6
      // 50(206) bis 100°(262)C:y = 0,8855x - 132
      // 100(263) bis 150°(334)C:y = 0,67,81x - 76,979
      // 150(335) bis 200°C(421):y = 0,5779x - 43,599
      // 200 (422) bis 250°C (502):y = 0,6061x - 54,361
      //250 (503) bis 280°C:y=0,6932x - 97,877
      
      // if (AvgTempVal[k] <= 205){
      // AvgTemp[k] = 1.06* AvgTempVal[k] - 168.6;
      // }
      // else if(AvgTempVal[k] > 205 && AvgTempVal[k] <= 262){
      //   AvgTemp[k] = 0.8855*AvgTempVal[k] - 132;
      // }
      // else if(AvgTempVal[k] > 262 && AvgTempVal[k] <= 334){
      //   AvgTemp[k] = 0.6781*AvgTempVal[k] -76.979;
      // }
      // else if(AvgTempVal[k] > 334 && AvgTempVal[k] <= 421){
      //   AvgTemp[k] = 0.5779*AvgTempVal[k] - 43.599;
      // }
      // else if(AvgTempVal[k] > 421 && AvgTempVal[k] <= 502){
      //   AvgTemp[k] = 0.1905*AvgTempVal[k] + 250.19;
      // }
      // else if(AvgTempVal[k] > 503){
      //   AvgTemp[k] = 0.6932*AvgTempVal[k] - 97.877;
      // }
    }
    Serial.println();
  }

  for (byte i = 0; i < 4; i++) {

    sollTemp[i] = readPoti(i) / 3;
    if (AvgTemp[i] < (sollTemp[i] - hyst / 2)) {
      digitalWrite(RelaisPins[i], HIGH);
    }
    if (AvgTemp[i] > (sollTemp[i] + hyst / 2)) {
      digitalWrite(RelaisPins[i], LOW);
    }
    display(i, AvgTemp[i], sollTemp[i]);


    delay(10);
  }

  //delay(100);
}
